import java.util.Random;
import java.util.Scanner;
public class guessinggame {
    public static void main (String[] args){
        System.out.println("Guess a number:");
        Random rand = new Random();
        int ran = rand.nextInt(10)+1;
        Scanner scan = new Scanner(System.in);
        int guess = scan.nextInt();
        int numGuess = 1;
        while (guess != ran){ 
            if (guess < ran){
                System.out.println("Too low, guess again:");
                guess = scan.nextInt();
                numGuess += 1;
            }
             if (guess > ran){
                System.out.println("Too high, guess again:");
                guess = scan.nextInt();
                numGuess += 1;
            }
        }
        
            System.out.println("Correct! It took you " + numGuess + " tries.");
            
}
}
